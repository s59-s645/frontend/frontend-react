import { useState, useEffect} from 'react';
import { Row, Col, Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom'

export default function ProductCard({productProp}) {


    const { name, description, price, _id } = productProp;

       const [count, setCount] = useState(0);
       const [stocks, setStocks] = useState(10);
   
       function orders(){
         if (stocks > 0 ) {
           setCount(count + 1)
           console.log('Counts: ' + count)
           setStocks(stocks - 1)
           console.log('Stocks: ' + stocks)
         } 
       };

       useEffect(()=>{
            if (stocks === 0) {
                alert("No more stocks available.")
            }

       },[stocks])
   

    return(
       
      
        <Col xs={12} md={4} >
            <Card className="cardProducts p-3 mb-3" >
                <Card.Body >
                   
                    <Card.Header className="text-center card-header">{name}</Card.Header>
                    <Card.Subtitle className="mt-4 text-">Description:</Card.Subtitle>
                    <Card.Text>{description}</Card.Text>
                    <Card.Subtitle>Price:</Card.Subtitle>
                    <Card.Text>{price}</Card.Text>
                    <Button className='bg-primary' as={ Link } to={`/products/${_id}`}>Details</Button>
                   
                </Card.Body>
            </Card>
        </Col>

        
      
    )
};
