import { Row, Col, Button } from "react-bootstrap";
import PageNotFound from './404';
function Banner(props) {
    const origin = props.origin;
  return (
    <>
        { origin === 'error' ? <PageNotFound/> :
            <Row>
            <Col className="p-5 mb-5">
            <div class="text-center">
              <h1>Melize Clothing</h1>
              <p>Shop Anywhere.</p>

              <Button variant="primary">Shop Now!</Button>
            </div>  
            </Col>
          </Row>
    
        }    
    </>
     
      
   
  );
}

export default Banner;


