import {Row, Col, Card} from 'react-bootstrap';

function Highlights(){
    return (
        <Row>
            <Col xs={12} md={4}>
                <Card className='cardHighlight p-3 mb-3'>
                    <Card.Body>
                        <Card.Title>Shop From Home</Card.Title>
                        <Card.Text>
                            You can choose your desire clothes
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className='cardHighlight p-3 mb-3'>
                    <Card.Body>
                        <Card.Title>Click Now! Pay Later</Card.Title>
                        <Card.Text>
                           Melize offers Cash On Delivery
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className='cardHighlight p-3 mb-3'>
                    <Card.Body>
                        <Card.Title>Choose your style</Card.Title>
                        <Card.Text>
                           You can choose your style in the palm of your hands right away!
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    );
}

export default Highlights;
