import { Fragment, useEffect, useState, useContext } from 'react';
import ProductCard from '../components/ProductCard';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';
import { Route } from 'react-router-dom';



export default function Products() {

	const [products, setProducts] = useState([]);

	const {user} = useContext(UserContext);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}products`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			setProducts(data.map(product => {
				return (
					<ProductCard  key={product._id} productProp={product} />
				)
			}))

		})
	}, [])	



	return (
		(user.isAdmin)
		?
			<Navigate to="/admin"/>
		:
			<Fragment>
				<h1 className="text-center my-3">Products</h1>
				{products}
			</Fragment>
	)
}

