import { useContext, useState, useEffect } from "react";
import {Table} from "react-bootstrap";
import {Navigate} from "react-router-dom";
import UserContext from "../UserContext";



export default function AllOrders(){


	const {user} = useContext(UserContext);
	const [allOrders, setAllOrders] = useState([]);

	const  fetchData = async () =>{

		await fetch(`${process.env.REACT_APP_API_URL}/order/allorders`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})

       
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllOrders(data.map(order => {
				return(
					<tr key={order._id}>
						<td>{order.userId}</td>
						<td>{order.productId}</td>
                        <td>{order.productPrice}</td>
						<td>{order.quantity}</td>
						<td>{order.totalAmount}</td>
						
					</tr>
				)
			}))

		})
	}


	
	useEffect(()=>{
		fetchData();
        console.log(fetchData);
	}, [])

    

	return(
		(user.isAdmin)
		?
		<>
			<div className="mt-5 mb-3 text-center">
				<h1>Orders Page</h1>
				
			</div>
			<Table striped bordered hover>
		     <thead>
		       <tr>
		         
		         <th>User ID</th>
                 <th>Product ID</th>
		         <th>Product Price</th>
		         <th>Quantity</th>
		         <th>TotalAmount</th>
		        
		       </tr>
		     </thead>
		     <tbody>
		       { allOrders }
		     </tbody>
		   </Table>
		</>
		:
		<Navigate to="/allOrders" />
	)
}
