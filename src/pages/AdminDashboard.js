import { useContext, useState, useEffect } from "react";
import {Table, Button} from "react-bootstrap";
import {Navigate, Link} from "react-router-dom";
import UserContext from "../UserContext";

import Swal from "sweetalert2";

export default function AdminDashboard(){

	
	const {user} = useContext(UserContext);


	const [allProducts, setAllProducts] = useState([]);

	const fetchData = () =>{
		
		fetch(`${process.env.REACT_APP_API_URL}products/all`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllProducts(data.map(product => {
				return(
					<tr key={product._id}>
						<td>{product.name}</td>
						<td>{product.description}</td>
						<td>{product.price}</td>



						<td>{product.isActive ? "Active" : "Inactive"}</td>
						<td>
							{
								(product.isActive)
								?	
								 	<>
									<Button variant="danger" size="sm" onClick ={() => archiveProduct(product._id, product.name)}>Archive</Button>

									<Button as={ Link } to={`/EditProduct/${product._id}`} variant="secondary" size="sm" className="m-2" >Edit</Button>

									</>
								:


										<Button variant="success" size="sm" onClick ={() => activeProduct(product._id, product.name)}>Unarchive</Button>


							}
						</td>
					</tr>
				)
			}))

		})
	}

	//Making the product inactive
	const archiveProduct = (productId, productName) =>{
		console.log(productId);
		console.log(productName);

		fetch(`${process.env.REACT_APP_API_URL}products/${productId}/archive`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			if(data){
				Swal.fire({
					title: "Archive Succesful!",
					icon: "success",
					text: `${productName} is now inactive.`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Archive Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	//Making the product active
	const activeProduct = (productId, productName) =>{
		console.log(productId);
		console.log(productName);

		fetch(`${process.env.REACT_APP_API_URL}products/active${productId}`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			if(data){
				Swal.fire({
					title: "Unarchive Succesful!",
					icon: "success",
					text: `${productName} is now active.`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Unarchive Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}


	useEffect(()=>{
		
		fetchData();
	}, [])

	return(
		(user.isAdmin)
		?
		<>
			<div className="mt-5 mb-3 text-center">
				<h1>Admin Dashboard</h1>

				<Button as={Link} to="/addProduct" variant="primary" size="lg" className="mx-2">Add product</Button>
{/*				<Button as={Link} to="/allOrders" variant="success" size="lg" className="mx-2">Show Orders</Button>*/}
			</div>
			<Table striped bordered hover>
		     <thead>
		       <tr>
		         <th>Product Name</th>
		         <th>Description</th>
		         <th>Price</th>


		         <th>Status</th>
		         <th>Action</th>
		       </tr>
		     </thead>
		     <tbody>
		       { allProducts }
		     </tbody>
		   </Table>
		</>
		:
		<Navigate to="/products" />
	)
}

