import {useEffect, useContext} from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Logout(){
    
    //consume the UserContext object and destructure it to access the setUser func and unsetUser func from the context provider
    const {unsetUser, setUser} = useContext(UserContext);

    //clear the localStorage of the user's information
    unsetUser();
    //localStorage.clear();

    //Placing the "setUser" function inside of a useEffect ois necessary because of updates within REACTJS that a state of another component cannot be updated while trying to render a different component

    //By adding useEffect, this will allow the logout page to render first before triggering the useEffect which changes the state of the user
    useEffect(() => {

        //set the user state back to it's original state
        setUser({id: null})
    }, [setUser])


    return(
        <Navigate to="/"/>

    )
};






