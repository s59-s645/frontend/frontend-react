import { Fragment,useState,useEffect,useContext } from 'react'
import { Form, Button } from 'react-bootstrap'
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';



export default function Register() {

  const navigate = useNavigate();
  const {user} = useContext(UserContext);
  
  //state hook
  const[email, setEmail] = useState(""); 
  const[mobileNo, setMobileNo] = useState("");
  const [password,setPassword] = useState("");
  const [password2,setPassword2] = useState("");

  
  //state to determine whether submit button is enable or not
  const [isActive, setIsActive] = useState(false);



     console.log(email);
     console.log(password);
     console.log(password2);



  function registerUser(e) {
   
    
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}users/checkEmail`, {
      method: 'POST',
      headers: {
        'Content-Type' : 'application/json',
      },
      body: JSON.stringify({
        email: email
      })

    })
    .then(res => res.json())
    .then(data => {
      console.log(data);
      if(data){
        Swal.fire({
          title: "Duplicate email found",
          icon: "info",
          text: "The email that you're trying to register already exist"
        })
      } else {
        fetch(`${process.env.REACT_APP_API_URL}users/register`,{
          method: 'POST',
          headers: {
            'Content-Type' : 'application/json',
          },
          body: JSON.stringify({
            email: email,
            mobileNo: mobileNo,
            password: password
          })
        })
        .then(res => res.json())
        .then(data => {
          console.log(data);

          if(data === true){

            // clear input fields
            setMobileNo('');
            setEmail('');
            setPassword('');
            setPassword2('');

            Swal.fire({
              title: 'Registration Successful!',
              icon: 'success',
              text: 'Thank you for registering'
            })

              navigate('/login')
          } else {
            Swal.fire({
              title: 'Registration failed',
              icon: 'error',
              text: 'Something went wrong, try again'
            })
          }
        })
      }
    })


  };

    
    

  //useEffect for button
  useEffect(() => {
    
    if((email !== '' && mobileNo.length === 11 && password !== '' && password2 !== '') && (  password === password2)){
        setIsActive(true);
    } else {
        setIsActive(false);
    }
}, [ email, mobileNo, password, password2]);


    return (
     (user.id !== null) ?
     <Navigate to="/products"/>
  
     :
    <Fragment>

    <h1 className='text-center mt-3'>Register</h1>
    <Form onSubmit={(e) => {registerUser(e)}}>
    <div class="border my-3 mb-3">
    <Form.Group className="mb-3" controlId="userEmail">
      <Form.Label>Email address</Form.Label>
      <Form.Control
        type="email"
        value = {email}
        placeholder="Enter your email here"
        onChange = {(e) => {setEmail(e.target.value)}}
                  required
      />

      <Form.Text className="text-muted">
        We'll never share your email with anyone else.
      </Form.Text>
    </Form.Group>

      {/* mobilenumber */}
    <Form.Group className="mb-3" controlId="mobileNo">
        <Form.Label>Mobile Number </Form.Label>
        <Form.Control
          type="text"
                    value = {mobileNo}
          placeholder="0999-999-9999"
          onChange = {(e) => {setMobileNo(e.target.value)}}
                    required
        />

      </Form.Group>

      <Form.Group className="mb-3" controlId="password">
        <Form.Label>Password</Form.Label>
        <Form.Control
          type="password"
                    value = {password}
          placeholder="Enter your Password"
          onChange = {(e) => {setPassword(e.target.value)}}
                    required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="password2">
        <Form.Label>Verify Password</Form.Label>
        <Form.Control
          type="password"
                    value = {password2}
          placeholder="Enter your Password Again"
          onChange = {(e) => {setPassword2(e.target.value)}}
                    required
        />
 
      </Form.Group> 
      </div> 


      <div class="text-center">
     { isActive ?
        <Button variant="success" type='submit' id='submitBtn' >
          Please enter your registration details
      </Button>
        :
        <Button variant="warning" type='submit' id='submitBtn' disabled>
        Please enter your registration details
      </Button>

    }
    <Form.Text className="text-muted">
      Already have an account? Click here to log in.
    </Form.Text>

    </div>
    </Form>
    
    </Fragment>
  )
}



